class CreateCompanyConfigurations < ActiveRecord::Migration[5.2]
  def change
    create_table :company_configurations do |t|
      t.json :json_store

      t.timestamps
    end
  end
end
