class CreateEmails < ActiveRecord::Migration[5.2]
  def change
    create_table :emails do |t|
      t.integer :resource_id
      t.string :resource_type
      t.string :email
      t.json :json_store
      t.string :status
      t.datetime :delivered_at

      t.timestamps
    end
  end
end
