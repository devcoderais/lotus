class CreateBasicSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :basic_settings do |t|
      t.string :label
      t.string :context
      t.string :resource
      t.string :data_type
      t.integer :company_id
      t.string :key
      t.boolean :enabled
      t.string :mandatory
      t.string :class_names
      t.boolean :hidden
      t.string :function

      t.timestamps
    end
  end
end
