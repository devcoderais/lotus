class CreateBulkUploadRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :bulk_upload_requests do |t|
      t.json :json_store

      t.timestamps
    end
  end
end
