class AddCcIdToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :cc_id, :integer
  end
end
