class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.string :name
      t.string :type
      t.string :status
      t.integer :score
      t.boolean :active

      t.timestamps
    end
  end
end
