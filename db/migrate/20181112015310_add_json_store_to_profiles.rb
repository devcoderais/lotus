class AddJsonStoreToProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :json_store, :json
  end
end
