class AddCompanyIdToProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :company_id, :integer
  end
end
