require 'test_helper'

class BulkUploadRequestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bulk_upload_request = bulk_upload_requests(:one)
  end

  test "should get index" do
    get bulk_upload_requests_url
    assert_response :success
  end

  test "should get new" do
    get new_bulk_upload_request_url
    assert_response :success
  end

  test "should create bulk_upload_request" do
    assert_difference('BulkUploadRequest.count') do
      post bulk_upload_requests_url, params: { bulk_upload_request: { json_store: @bulk_upload_request.json_store } }
    end

    assert_redirected_to bulk_upload_request_url(BulkUploadRequest.last)
  end

  test "should show bulk_upload_request" do
    get bulk_upload_request_url(@bulk_upload_request)
    assert_response :success
  end

  test "should get edit" do
    get edit_bulk_upload_request_url(@bulk_upload_request)
    assert_response :success
  end

  test "should update bulk_upload_request" do
    patch bulk_upload_request_url(@bulk_upload_request), params: { bulk_upload_request: { json_store: @bulk_upload_request.json_store } }
    assert_redirected_to bulk_upload_request_url(@bulk_upload_request)
  end

  test "should destroy bulk_upload_request" do
    assert_difference('BulkUploadRequest.count', -1) do
      delete bulk_upload_request_url(@bulk_upload_request)
    end

    assert_redirected_to bulk_upload_requests_url
  end
end
