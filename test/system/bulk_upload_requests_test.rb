require "application_system_test_case"

class BulkUploadRequestsTest < ApplicationSystemTestCase
  setup do
    @bulk_upload_request = bulk_upload_requests(:one)
  end

  test "visiting the index" do
    visit bulk_upload_requests_url
    assert_selector "h1", text: "Bulk Upload Requests"
  end

  test "creating a Bulk upload request" do
    visit bulk_upload_requests_url
    click_on "New Bulk Upload Request"

    fill_in "Json Store", with: @bulk_upload_request.json_store
    click_on "Create Bulk upload request"

    assert_text "Bulk upload request was successfully created"
    click_on "Back"
  end

  test "updating a Bulk upload request" do
    visit bulk_upload_requests_url
    click_on "Edit", match: :first

    fill_in "Json Store", with: @bulk_upload_request.json_store
    click_on "Update Bulk upload request"

    assert_text "Bulk upload request was successfully updated"
    click_on "Back"
  end

  test "destroying a Bulk upload request" do
    visit bulk_upload_requests_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Bulk upload request was successfully destroyed"
  end
end
