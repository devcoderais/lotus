module BulkUploadSetting

  def bulk_upload_default_fields
    # Fetch model attributes
    # config = YAML.load_file("#{Rails.root.to_s}/config/bulk_upload_settings.yml")["defaults"]["#{self.class&.underscore}"]
    config = YAML.load_file("#{Rails.root.to_s}/config/bulk_upload_settings.yml")["defaults"]
  end

  def basic_facts_fields
    YAML.load_file("#{Rails.root.to_s}/config/basic_facts.yml")["defaults"]
  end

  def profile_bulk_upload_params
    params.require(:profile).permit(:first_name, :last_name, :email, :blood_group, :sll, :bm)
  end

end