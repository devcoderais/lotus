require 'active_support/concern'

module ExcelReader
  extend ActiveSupport::Concern
  module ClassMethods
    def initiate_bulk_upload(data, options={})
      # debugger
      begin
      ActiveRecord::Base.transaction do
        hash = {}
        company_id = options[:company_id]
        company = Company.find_by_id company_id
        configuration = company.configuration
        model_name = options[:model_name]
        model_object = options[:model_name].classify.constantize.new
        model_object_params = model_object.send("#{options[:model_name]}_bulk_upload_params")
        json_key = model_name+'_bu_headers_sequence'
        sequence = configuration.send(json_key.to_s)
        file_path = data[:bulk_upload].path rescue nil
        # For filed uploaded remotely
        # Creek::Book.new remote_url, remote: true
        if (!file_path.blank?)
          creek = Creek::Book.new file_path
          # For single Sheet
          sheet = creek.sheets[0]
          sheet.simple_rows.each do |row|
            puts "*"*20
            puts row
            puts "*"*20

            row.sort.each.with_index(1) do |(key, value), index|
              hash.merge!({"#{sequence.key(index.to_s)}" => value})
            end
            model_object(model_object_params)
            model_object.save!
          end
        end
        # final hash will be params like structure for general and nested fields
        puts '%'*10
        puts hash
        puts '%'*10
        return hash
      end
      rescue => e
        puts e.backtrace
      end
    end
  end
end