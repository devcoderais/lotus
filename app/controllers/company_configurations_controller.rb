class CompanyConfigurationsController < ApplicationController

  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
  end

  def update
    @company = Company.find_by_id(params[:company_id])
    @company_configuration = CompanyConfiguration.find_by_id(params[:id])
    scope = params[:scope]
    case scope
    when *@company_configuration.bulk_upload_default_model_field_sequence
      if (!params[scope].blank? && @company_configuration.update("#{scope}".to_sym => params[scope]))
        redirect_path = configuration_company_path(@company)
        status = :ok
        message_type = "notice"
        message = "Update Sucessful!"
        render = @company_configuration
      else
        redirect_path, status, message_type, message, render = @company_configuration.update_error(@company.id)
      end
    else
      redirect_path, status, message_type, message, render = @company_configuration.update_error(@company.id)
    end
    respond_to do |format|
      format.html {redirect_to redirect_path, "#{message_type}": message}
      format.json {render json: render, status: status }
    end
  end

  def destroy
  end


  private

  def company_configuration_params
    params.require(:company_configuration).permit(profile_bu_headers_sequence: {}, report_bu_headers_sequence: {})
  end

end