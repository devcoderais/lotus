class BulkUploadRequestsController < ApplicationController
  before_action :set_bulk_upload_request, only: [:show, :edit, :update, :destroy]

  # GET /bulk_upload_requests
  # GET /bulk_upload_requests.json
  def index
    @bulk_upload_requests = BulkUploadRequest.all
  end

  # GET /bulk_upload_requests/1
  # GET /bulk_upload_requests/1.json
  def show
  end

  # GET /bulk_upload_requests/new
  def new
    @bulk_upload_request = BulkUploadRequest.new
    @companies = Company.all
    @model_names = @bulk_upload_request.bulk_upload_default_fields.keys
  end

  # GET /bulk_upload_requests/1/edit
  def edit
  end

  # POST /bulk_upload_requests
  # POST /bulk_upload_requests.json
  def create
    # debugger
    company = Company.find_by_id(params[:company_id])
    @bulk_upload_request = BulkUploadRequest.new(bulk_upload_request_params)
    if !params[:model_name].blank?
      klass = model_object = params[:model_name].classify.constantize
      klass.handle_bulk_upload(params, {company_id: params[:company_id], model_name: params[:model_name]})
    end
    respond_to do |format|
      if @bulk_upload_request.save
        format.html { redirect_to @bulk_upload_request, notice: 'Bulk upload request was successfully created.' }
        format.json { render :show, status: :created, location: @bulk_upload_request }
      else
        format.html { render :new }
        format.json { render json: @bulk_upload_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bulk_upload_requests/1
  # PATCH/PUT /bulk_upload_requests/1.json
  def update
    respond_to do |format|
      if @bulk_upload_request.update(bulk_upload_request_params)
        format.html { redirect_to @bulk_upload_request, notice: 'Bulk upload request was successfully updated.' }
        format.json { render :show, status: :ok, location: @bulk_upload_request }
      else
        format.html { render :edit }
        format.json { render json: @bulk_upload_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bulk_upload_requests/1
  # DELETE /bulk_upload_requests/1.json
  def destroy
    @bulk_upload_request.destroy
    respond_to do |format|
      format.html { redirect_to bulk_upload_requests_url, notice: 'Bulk upload request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def configurations
    @bulk_upload_request = BulkUploadRequest.new
    @company = Company.find_by_id(params[:company_id])
    @configuration = @company.configuration if !@company.blank?
    selected_model = params[:model_type]
    @bulk_upload_default_model_fields = @bulk_upload_request.specific_model_default_fields(selected_model)
    respond_to do |format|
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bulk_upload_request
      @bulk_upload_request = BulkUploadRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bulk_upload_request_params
      params.permit(:bulk_upload_request, :json_store)
    end
end
