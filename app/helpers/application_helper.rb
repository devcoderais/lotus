module ApplicationHelper
  extend ActionView::Helpers::TextHelper
  class MarkedFormBuilder < ActionView::Helpers::FormBuilder
    def marked_label(method, text = nil, options = {}, &block)
      addon = object.class.validators_on(method).map(&:class).include?(ActiveRecord::Validations::PresenceValidator) ? "*" : ""
      label(method, text, options, &block) + addon
    end
  end
end
