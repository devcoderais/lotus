require 'bulk_upload_setting.rb'
class BulkUploadRequest < ApplicationRecord
  include BulkUploadSetting

  def specific_model_default_fields(model_type)
    self.bulk_upload_default_fields.slice(model_type)    
  end

end
