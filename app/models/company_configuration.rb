require 'bulk_upload_setting.rb'
class CompanyConfiguration < ApplicationRecord

  # *********************MIXINS START*******************************************
  include BulkUploadSetting
  # *********************MIXINS END********************************************

  # *********************CONSTANT DEFINITION START***********************************
  # profile_bu_headers_sequence - Profile Bulk Upload Column Sequence

  JSON_ACCESSORS = [:profile_bu_headers_sequence, :report_bu_headers_sequence]
  store_accessor :json_store, *JSON_ACCESSORS
  # *********************CONSTANT DEFINITION END*************************************

  # *********************TRIGGER DEFINITION START************************************
  after_initialize :init
  # *********************TRIGGER DEFINITON END***************************************

  # *********************TRIGGER METHOD DEFINITION START*****************************
  def init
    self.profile_bu_headers_sequence ||= {}
    self.report_bu_headers_sequence ||= {}
  rescue ActiveModel::MissingAttributeError
    # This happens on Model.select calls when these attributes might not be loaded. It can be safely ignored.  
  end
  # *********************TRIGGER METHOD DEFINITION END*******************************

  def update_error(company_id=nil)
    company = Company.find_by_id(company_id)
      redirect_path = company.blank? ? Rails.application.routes.url_helpers.companies_path
 : Rails.application.routes.url_helpers.configuration_company_path(company)
      status = :unprocessable_entity
      message_type = "alert"
      message = "Some error occured. Please try again!"
      render = self
    return [redirect_path, status, message_type, message,  render]
  end

  def bulk_upload_default_model_field_sequence
    return ["profile_bu_headers_sequence", "report_bu_headers_sequence"]
  end

end
