class Company < ApplicationRecord


  # *********************RELATIONSHIP DEFINITION START*******************************
  belongs_to :configuration, class_name: "CompanyConfiguration", foreign_key: :cc_id  
  has_many :profiles
  # *********************RELATIONSHIP DEFINITION END*********************************

end
