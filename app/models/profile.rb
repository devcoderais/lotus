require 'bulk_upload_setting.rb'
include ExcelReader

class Profile < ApplicationRecord

  # *********************MIXINS START*******************************************
  include BulkUploadSetting
  # *********************MIXINS END********************************************
  
  belongs_to :company
  validates_presence_of :first_name, :email
  validates_format_of :email,:with => Devise::email_regexp 
  # *********************CONSTANT DEFINITION START***********************************
  # sll - Secondary Landline number
  #bm - Birth Mark
  JSON_ACCESSORS = [:sll, :bm]
  store_accessor :json_store, *JSON_ACCESSORS
  # *********************CONSTANT DEFINITION END*************************************

  # *********************TRIGGER DEFINITION START************************************
  after_initialize :init
  # *********************TRIGGER DEFINITON END***************************************

  # *********************TRIGGER METHOD DEFINITION START*****************************
  def init
    self.sll ||= ""
    self.bm ||= ""
  rescue ActiveModel::MissingAttributeError
    # This happens on Model.select calls when these attributes might not be loaded. It can be safely ignored.  
  end
  # *********************TRIGGER METHOD DEFINITION END*******************************

  def self.handle_bulk_upload(data, options={})
    Profile.initiate_bulk_upload(data, options={})
  end

end
