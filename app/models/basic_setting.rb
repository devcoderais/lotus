class BasicSetting < ApplicationRecord

  validates_presence_of :label, :resource, :data_type, :key
  validates_uniqueness_of :key
  
  DATA_TYPES = %w{integer float text boolean string time datetime date select multiselect}
  CONTEXTS = %w{profile report}
  DATA_TYPE_MAPPING = {:text => "text", :select => "radio", :boolean => "truth"}
  TYPE_MANDATORY = ['first_name', 'bm']

  before_validation :pre_validation_sanitize

  def pre_validation_sanitize
    self.key = label.tableize.singularize if key.blank? && !label.blank?
  end


  # basic_facts.yml contains global definitions for key and values.
  # Override basic facts by adding record with same key
  # Override form label by using mark_label in ApplicationHelper
end
