// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require bootstrap-sprockets
//= require jquery-ui
//= require activestorage
//= require turbolinks
//= require_tree .




$(document).on('ready turbolinks:load', onPageReadyPlugins);

function onPageReadyPlugins(){


  $(".select_all").on('change' ,function(event) {
    var type = $(this).data("conf");
    $('.'+type+'_selectable').prop('checked', $(this).is(':checked'));
    checked_attributes = $('.'+type+'_selectable:checked');
    unchecked_attributes = $('.'+type+'_selectable').not(':checked')
    if($(this).is(':checked')){
      $.each(checked_attributes, function(e){
        drag_drop_sort($(this));
      });
    }
    else{
      $.each(unchecked_attributes, function(e){
        drag_drop_sort($(this));
      });
    }
    event.stopImmediatePropagation();
    event.preventDefault();
  });

  $(".selectable").on('click', function(event){
    set_select_all($(this));
  });

  $('.sortable_checkbox').on('change',function(event){

    drag_drop_sort($(this));
    event.stopImmediatePropagation();
    event.preventDefault();
  });

    var fixedHelperModified = function(e, tr) {
      var $originals = tr.children();
      var $helper = tr.clone();
      $helper.children().each(function(index) {
          $(this).width($originals.eq(index).width())
      });
      return $helper;
    }

    $('.ui-sortable').sortable({
      helper: fixedHelperModified,
      stop: function(event,ui) {
        // debugger
        // list_div_id = $(this).parent().attr('id');
        list_div_id = $(this).data("conf")+'_list_fields'
        renumber_table('table.'+list_div_id)}
    }).disableSelection();
  // });

  $('form.bu_settings').on('submit',function(){
    conf = $(this).data("conf");
  var drag_drop_table = $('.'+conf+'_drag_drop');
    $.each(drag_drop_table, function(){
      cb_array = $('input:checkbox[id^="'+conf+'_"]:checked');
      $.each(cb_array, function(){
        id = $(this).attr('id');
        priority = $("td#"+ id).prev('td.priority').text();
        $(this).val(priority);
      });
    });
  });

  $("#model_name").on('change', function(event){
    company_id = $("#company_id").val();
    selected_model = $("#model_id").val();
    $.ajax({
      url: "configurations.js",
      data: {"company_id": company_id, "model_type": selected_model}
    });
  });
}

function add_to_list(item, item_id, list_div_id){
  var label = item.next('label').text();
 var  count = $('table.'+list_div_id+' .ui-sortable').children().length;
  var count = count + 1;
  $('table.'+list_div_id+' .ui-sortable').append("<tr><td class='priority'>"+ count +"</td><td id="+ item_id +  ">"+ label +"</td></tr>");
}


function remove_from_list(item){
  var id = item.attr('id');
  $('tr:has(td#'+ id +')').remove();
}


//Renumber table rows
function renumber_table(tableID) {
  $(tableID + " tr").each(function() {
  count = $(this).parent().children().index($(this)) + 1;
  $(this).find('.priority').html(count);
  td = $(this).find('td:eq(1)');
  prtd = td.prev('td.priority');
  val = td.text();
  id = td.attr('id')
  priority = prtd.text();
  });
}

function set_select_all(thisObj){
  var type = thisObj.data("conf");
  var selectable_item = '.'+type+'_selectable'
  if($(selectable_item).length == $(selectable_item+":checked").length) {
    $('#'+type+'_select_all').prop("checked", true);
  } else {
    $('#'+type+'_select_all').prop("checked", false);
  }
}

function drag_drop_sort(thisObj){
  var id = thisObj.closest('.row').attr('id');
  var list_div_id = thisObj.data("list-id");
  if( thisObj.is(':checked') ){
    item = thisObj;
    item_id = item.attr('id');
    add_to_list( item, item_id, list_div_id);
  }
  else{
    remove_from_list( thisObj );
  }
  renumber_table('table.'+list_div_id);
  // set_select_all();
}