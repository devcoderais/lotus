json.extract! bulk_upload_request, :id, :json_store, :created_at, :updated_at
json.url bulk_upload_request_url(bulk_upload_request, format: :json)
