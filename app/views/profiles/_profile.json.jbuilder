json.extract! profile, :id, :first_name, :last_name, :email, :blood_group, :created_at, :updated_at
json.url profile_url(profile, format: :json)
