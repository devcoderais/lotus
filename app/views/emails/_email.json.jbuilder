json.extract! email, :id, :resource_id, :resource_type, :email, :json_store, :status, :delivered_at, :created_at, :updated_at
json.url email_url(email, format: :json)
