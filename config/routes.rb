Rails.application.routes.draw do
  resources :emails
  resources :bulk_upload_requests do
    collection do 
      get 'configurations'
    end
  end
  root 'companies#index'
  resources :companies do
    member do
      get 'configuration'
    end
  end
  devise_for :users
  root 'profiles#index'  
  resources :profiles
  resources :company_configurations

end